'use strict';
var config = require('config');

var SwaggerExpress = require('swagger-express-mw');
var app = require('express')();

var mongoose = require('mongoose');
mongoose.connect('mongodb://mongo/'+config.get('app.name'), {
    reconnectTries: 6,
    reconnectInterval: 10000
});

SwaggerExpress.create({appRoot: __dirname}, function(error, swaggerExpress) {
    if (error) { throw error; }
    swaggerExpress.register(app);
    app.listen(80);
});