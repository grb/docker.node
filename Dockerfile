#++++++++++++++++++++++++++++++++++++++
# Node.js Docker container
#++++++++++++++++++++++++++++++++++++++

FROM node:9.4

WORKDIR /usr/src/app
COPY ./app/ ./

RUN npm install -g forever
RUN npm install

EXPOSE 80
CMD [ "npm", "start" ]