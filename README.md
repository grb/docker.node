# Node.js Boilerplate #

This is an easy customizable Node.js docker boilerplate.

##Supports##

* Node.js
* MongoDB
* Swagger

maybe more later...

This Docker boilerplate is based on the [Docker best practices](https://docs.docker.com/engine/userguide/eng-image/dockerfile_best-practices/) and doesn't use too much magic. Configuration of each docker container is available in the docker/ directory - feel free to customize.

This boilerplate can also be used for any other web project. Just customize the makefile for your needs.
Warning: There may be issues when using it in production.
If you have any success stories please contact me.

## Installation ##

```
#!bash

mkdir newProject
cd newPrject
git clone git@bitbucket.org:grb/docker.node.git .
```

Customize the docker-compose.yml file for your needs and start the project:

```
#!bash

docker-compose up -d && docker-compose logs -f
```

## Rename APP-NAME ##
Find and replace "APP-NAME" with your app name. Prefix "ms-" (for microservice) is NOT needed.

## Rename PROJECTNAME ##
Find and replace "PROJECTNAME" with your project name. Example "customerxy"
